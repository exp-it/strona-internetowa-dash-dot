(function($) {
	$.fn.lpTooltip = function(options) {
		var mainWidth = $(window).width();
  	var settings = $.extend( {
		  vertical : 'top',
		  horizontal : 'left',
		  vectorX: 0,
		  textTip: null,
		}, options);
		
		function mouseenter(event) {
        $this = $(this);
        showTooltip($this) ;
        return true;
    }
    
    function mouseleave(event) {
        $this = $(this);
        //off tooltip
        //removeTooltip();
        return true;
    }
    
    function onClick(event) {
        $this = $(this);
        showTooltip($this) ;
        return true;
    }
    
    function showTooltip(tip) {
    	removeTooltip();
    	//$this = $(this);
			var iw, vx, vy, ay;
			var coords = tip.attr('coords');
			var vx = tip.attr('vector-x');
			var vy = tip.attr('vector-y');
			coords = coords.split(',');
			var x = coords[0];
			var y = coords[1];
			var container = tip.parent().parent();
			var imageContainer = container.find('.lpTImage');
			container.append('<div id="lpTt"></div>');
			imageContainer.append('<div id="lpArrow"></div>')
			var tt = $('#lpTt');
			var ttImage = $('#lpArrow');
			
			
			ttImage.addClass(settings.horizontal+'-v').addClass(settings.vertical+'-h');
			
			tt.addClass('lpTt').html(settings.textTip);
    	iw = tt.width();
    	
    	//for right
    	var ax = parseInt((coords[2] - coords[0])/2) + parseInt(coords[0]);
    	if(settings.horizontal=='left') {
				ax = ax - 77;	
			}
			
			ax = parseInt(ax) + parseInt(vx);
			ay = parseInt(y) + parseInt(vy);
			
			ttImage.css('left', ax+'px').css(settings.vertical,ay+'px');
    	x = 30;
    	y = parseInt(y) + 20;
    	tt.css(settings.horizontal, x+'px').css(settings.vertical,y+'px');
		}
		
		function removeTooltip() {
			$('#lpTt').remove();
			$('#lpArrow').remove();
		}
		
		var methods = {
        lpTooltip: function() {
            $this = $(this);
            data = $this.data("vertical");
            
        },
        destroy: function() {
            $this = $(this);
            $this.unbind("click");    
        }
    };
    
    return this.each(function() {
    	$this = $(this);
    	$this.bind("mouseenter", mouseenter);
    	$this.bind("mouseleave", mouseleave);
    	$this.bind("click", onClick);
    	
    });
    
	}
})(jQuery);

$(document).ready(function() {
	$('.lpTooltip').each(function() {
		$(this).lpTooltip({
			textTip: $(this).next('div').html()
		});
	});
	
	$('.lpTooltipRight').each(function() {
		$(this).lpTooltip({
			horizontal: 'right',
			textTip: $(this).next('div').html()
		});
	});
	
	$('.lpTooltipRightDown').each(function() {
		$(this).lpTooltip({
			horizontal: 'right',
			vertical: 'bottom-right',
			textTip: $(this).next('div').html()
		});
	});

	$('.lpTooltipLeftDown').each(function() {
		$(this).lpTooltip({
			vertical: 'bottom-left',
			textTip: $(this).next('div').html()
		});
	});	
	
});