<div class="lpTContainer">
<div class="lpTImage"><img src="img/dot.jpg" class="img-responsive" usemap="#dot_map" alt="" /></div>
<map name="dot_map" id="dot_map">
<area class="lpTooltipLeftDown" id="bateria" href="#dot" shape="rect" coords="101,243,224,351" style="outline:none;" target="_self"  vector-x="0" vector-y="0"/>
<div class="hidden"><p>Bateria z możliwością<br/>doładowania za pomocą dołączonego<br/>kabla z końcówką mikro USB.</p></div>

<area class="lpTooltip" id="ir" href="#dot" shape="rect" coords="0,112,82,205" style="outline:none;" target="_self"  vector-x="0" vector-y="-30" />
<div class="hidden"><p>4 nadajniki IR do komunikowania<br/>się z innymi robotami</p></div>

<area class="lpTooltip" id="oko" href="#dot" shape="rect" coords="82,80,227,228" style="outline:none;" target="_self"  vector-x="0" vector-y="0"/>
<div class="hidden"><p>Światła oka z 16 LEDami<br/>do zaprogramowania</p></div>

<area class="lpTooltip" id="glosnik" alt="glosnik" href="#dot" shape="rect" coords="0,3,79,105" style="outline:none;" target="_self"  vector-x="0" vector-y="0" />
<div class="hidden"><p>Głośniki i system audio,<br/>który przechowuje klipy dzwiękowe;<br/>aktualizowane bezprzewodowo</p></div>

<area class="lpTooltip" id="miernik" alt="miernik" href="#dot" shape="rect" coords="83,0,229,77" style="outline:none;" target="_self"  vector-x="0" vector-y="-30" />
<div class="hidden"><p>Miernik przyspieszenia,<br/>który umożliwia rozpoznanie ruchu.</p></div>

<area class="lpTooltipRight" id="przyspieszenie" alt="przyspieszenie" href="#dot" shape="rect" coords="233,0,326,115" style="outline:none;" target="_self"  vector-x="0" vector-y="0"/>
<div class="hidden"><p>Miernik przyspieszenia,<br/>który umożliwia rozpoznanie ruchu.</p></div>

<area class="lpTooltipRight" id="uszy" alt="uszy" href="#dot" shape="rect" coords="233,117,326,160" style="outline:none;" target="_self"  vector-x="0" vector-y="-30"/>
<div class="hidden"><p>2 kolorowe światła uszu</p></div>

<area class="lpTooltipRight" id="akcesoria" alt="akcesoria" href="#dot" shape="rect" coords="237,168,326,296" style="outline:none;" target="_self"  vector-x="0" vector-y="0"/>
<div class="hidden"><p>3 punkty łączenia na zamontowanie<br/>akcesoriów</p></div>

</map>
</div>