<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Dash i Dot</title>
  <meta name="description" content="Dash i Dot">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
  <link rel="stylesheet" href="css/b4/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">

  <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
  <![endif]-->
</head>
<body>
<div id="menu-top" class="active"><a class="hamburger-icon" id="menuButton"><i class="fa fa-bars"></i></a>
	<div class="mobile-nav">
    <div class="nav-items">
      <h3 class="active"><a href="index.php" target="_self">Strona główna</a></h3>
		  <h3><a href="#dash" target="_self">Dash</a></h3>
		  <h3><a href="#dot" target="_self">Dot</a></h3>
		  <h3><a href="http://wonderpolska.pl/" class="nav-link" target="_blank">Sklep</a></li></h3>		 
		  <h3><a href="http://makewonder.pl/kontakt.html" class="nav-link" target="_blank">Kontakt</a></h3>
		  <h3><a href="tel:534-222-422" target="_blank">tel: 534-222-422</a></h3>
		 
    </div>
  </div>
	<div class="container">
  	<nav class="navbar">
    	<ul>
    		<li><a href="index.php" target="_self" class="logo">Wonder Workshop</a></li>
				<li><a href="#dash" class="nav-link">Dash</a></li>
				<li><a href="#dot" class="nav-link">Dot</a></li>
				<li><a href="http://wonderpolska.pl/" class="nav-link" target="_blank">Sklep</a></li>
				<li><a href="http://makewonder.pl/kontakt.html" class="nav-link" target="_blank">Kontakt</a></li>
				<li><a href="http://makewonder.pl/kontakt.html" class="nav-link" target="_blank">tel: 534-222-422</a></li>
			</ul>
    </nav>
  </div>
</div>

<div class="page page1"><div class="slogan"><h1>Szukasz<br/>prezentu innego<br/>niż wszystkie?</h1></div></div>
<div class="page-content blue"><p>Chcesz podarować dziecku coś co nie tylko sprawi radość,<br/>
 ale również czegoś nauczy?<br/>
A może poszukujesz sposobu, by odciągnąć dziecko od wirtualnej<br/>
rzeczywistości, jednocześnie nie pozbawiając go możliwości bycia<br/>
na bieżąco z nowoczesnymi technologiami?</p></div>
<div class="page-content orange arrow-right arrow-left"><p>Jeżeli, na któreś z tych pytań odpowiadasz twierdząco,<br/>
to zapraszamy do poznania robotów Dash i Dot, które stanowią<br/>
potężne interaktywne narzędzie edukacyjne w przebraniu robotów,<br/>
 z którymi czas upływa na świetnej zabawie. Niewykluczone,<br/>
że dzięki nim Twoje dziecko odnajdzie pasje związaną<br/>
z  najbardziej przyszłościową dziedziną na świecie.</p></div>
<div class="page " id="dash">
	<h2>Dash, to większy z robotów</h2>
	<h3>Może funkcjonować sam lub z Dot’em. Poznaj jego możliwości.</h3>
	<div class="page-center"><?php include 'dash.map.php'; ?></div>
</div>
<div class="page " id="dot">
	<h2>Dot, robot który się nie rusza?</h2>
	<h3>Tak, nie czyni to jednak go bezużytecznym. Sam w sobie ma wielkie możliwości,<br/>
a w połączeniu z Dash’em tworzą duet zapewniający setki godzin zabawy,<br/>
a ich możliwości ogranicza tylko wyobraźnia – jak wiemy dziecięca wyobraźnia nie zna granic!</h3>
	<div class="page-center"><?php include 'dot.map.php'; ?></div>
</div>

<div class="page-content blue">
	<h1>Jak to działa?</h1>
	<p>Do rozpoczęcia przygody potrzebujesz Dash’a, Dot’a lub obydwóch robotów,<br/>
tabletu, czy notebooka z bluetooth’em i jednej spośród darmowych aplikacji.<br/><br/>
W tym momencie rozpoczyna się zabawa, w trakcie której – nim się<br/>
obejrzysz – zarówno ty, jak i dziecko od prostego sterowania Dash’em<br/>
 przejdziecie do bardziej skomplikowanych procesów programowania.<br/>
 Aplikacje zostały stworzone tak, aby świat programowania stanął otworem<br/>
przed osobami, dla których jest to zupełnie obca dziedzina.  Dash i Dot są<br/>
jednak również świetnym narzędziem dla doświadczonych miłośników<br/>
kodowania, którzy chcą zarażać dzieci programistycznym bakcylem.</p>
</div>

<div class="page page2"><div class="slogan"><h1>Przekonacie się sami,<br/>że od fabularyzowanych misji<br/>zawartych w aplikacjach<br/>trudno się oderwać.</h1></div></div>

<div class="page-content grey container">
	<div class="row">
		<div class="col-xs-12 col-md-6"><div class="pill-background bblue"></div><div class="pill pblue"><a href="http://www.makewonder.pl" target="_blank"><h2>dowiedz się więcej</h2><p>www.makewonder.pl</p></a></div></div>
		<div class="col-xs-12 col-md-6"><div class="pill-background borange"></div><div class="pill orange"><a href="http://www.wonderpolska.pl" target="_blank"><h2>chcę własnego robota</h2><p>www.wonderpolska.pl</p></a></div></div>
		
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script>
<script src="js/b4/bootstrap.js"></script>
<script src="js/script.js"></script>
<script src="js/lpTooltip.js"></script>
<!--<script src="js/jquery.rwdImageMaps.js"></script>-->
</body>
</html>