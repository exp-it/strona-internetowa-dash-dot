<div class="lpTContainer">
<div class="lpTImage"><img id="dash" src="img/dash.jpg" class="img-responsive" usemap="#dash_map" alt="" /></div>
<map name="dash_map" id="dash_map">
<area id="silnik" class="lpTooltip" alt="" href="#dash" shape="rect" coords="165,0,302,30" style="outline:none;" target="_self" vector-x="0" vector-y="-50"/>
<div class="hidden"><p>Głośniki i system audio,<br/>który przechowuje klipy dzwiękowe;<br/>aktualizowane bezprzewodowo</p></div>

<area id="uszy" class="lpTooltip" alt="" href="#dash" shape="rect" coords="0,29,163,162" style="outline:none;" target="_self" vector-x="40" vector-y="0" />
<div class="hidden"><p>Kolorowe światła uszu i czujniki dźwięku</p></div>

<area id="oko" class="lpTooltip" alt="" href="#dash" shape="rect" coords="168,31,288,161" vector-x="0" vector-y="0"/>
<div class="hidden"><p>Światła oka z 16 LEDami<br/>do zaprogramowania</p></div>

<area id="uszy" alt="uszy" class="lpTooltipRight" href="#dash" shape="rect" coords="290,33,460,160" style="outline:none;" target="_self" vector-x="-50" vector-y="0" />
<div class="hidden"><p>Kolorowe światła uszu i czujniki dźwięku</p></div>

<area id="sensory" class="lpTooltip" alt="sensory" href="#dash" shape="rect" coords="0,166,166,240" style="outline:none;" target="_self" vector-x="-10" vector-y="-10"/>
<div class="hidden"><p>Sensory do wykrywania<br/>obiektów i ścian z tyłu</p></div>

<area id="nadajnik" class="lpTooltipRight" alt="nadajnik" href="#dash" shape="rect" coords="287,167,460,238" style="outline:none;" target="_self" vector-x="0" vector-y="0"/>
<div class="hidden"><p>Nadajnik IR i czujniki podczerwieni<br/>do wykrywania innych robotów.<br/>Miernik przyspieszenia i żyroskop do ustalenia pozycji.</p></div>

<area id="bateria" class="lpTooltipLeftDown" alt="bateria" href="#dash" shape="rect" coords="148,323,325,402" style="outline:none;" target="_self" vector-x="-10" vector-y="-90"/>
<div class="hidden"><p>Bateria z możliwością<br/>doładowaniaza pomocą dołączonego<br/>kabla z końcówką mikro USB.</p></div>

<area id="kola" class="lpTooltipLeftDown" alt="kola" href="#dash" shape="rect" coords="0,322,147,402" style="outline:none;" target="_self" vector-x="0" vector-y="0" />
<div class="hidden"><p>Urządzenie kodujące do śledzenia ruchu kół,<br/>umożliwiające precyzyjną kontrolę</p></div>

<area id="mocowanie" class="lpTooltip" alt="mocowanie" href="#dash" shape="rect" coords="0,243,148,320" style="outline:none;" target="_self" vector-x="-50" vector-y="0"/>
<div class="hidden"><p>6 punktów łączenia<br/>do zamontowania akcesoriów.</p></div>

<area id="sensory2" class="lpTooltipRight" alt="sensory2" href="#dash" shape="rect" coords="327,242,460,318" style="outline:none;" target="_self" vector-x="50" vector-y="-30"/>
<div class="hidden"><p>Sensory do wykrywania<br/>obiektów i ścian z tyłu</p></div>

<area id="ir" class="lpTooltipRight" alt="ir" href="#dash" shape="rect" coords="148,243,326,320" style="outline:none;" target="_self" vector-x="0" vector-y="0"/>
<div class="hidden"><p>Sensory do wykrywania<br/>obiektów i ścian z przodu</p></div>

<area id="kola2" class="lpTooltipRightDown" alt="kola2" href="#dash" shape="rect" coords="324,323,460,402" style="outline:none;" target="_self" vector-x="30" vector-y="-30"/>
<div class="hidden"><p>Urządzenie kodujące do śledzenia<br/>ruchu kół, umożliwiające precyzyjną kontrolę</p></div>

</map>
</div>